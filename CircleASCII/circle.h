#ifndef CIRCLE_H
#define CIRCLE_H

void drawCircle(unsigned int r);
bool isFillOrEdge(int x, int y, int r);
bool isFill(int x, int y, int r);

#endif