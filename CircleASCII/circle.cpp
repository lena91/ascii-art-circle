#include "circle.h"
#include <iostream>
#include <windows.h>

void drawCircle(unsigned int r)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	for (int x = -static_cast<int>(r); x <= static_cast<int>(r); ++x)
	{
		for (int y = -static_cast<int>(r); y <= static_cast<int>(r); y++)
		{
			if (isFillOrEdge(x, y, r))
			{
				if (isFill(x, y, r))
				{
					SetConsoleTextAttribute(hConsole, 12);
					std::cout << "+";
				}
				else
				{
					SetConsoleTextAttribute(hConsole, 10);
					std::cout << "x";
				}
			}
			else
				std::cout << " ";
		}
		std::cout << "\n";
	}
	SetConsoleTextAttribute(hConsole, 15);
}

bool isFillOrEdge(int x, int y, int r)
{
	if ((x * x) + (y * y) < (r * r))
		return true;
	else
		return false;
}

bool isFill(int x, int y, int r)
{
	if (isFillOrEdge(x, y + 1, r) && isFillOrEdge(x, y - 1, r) && isFillOrEdge(x + 1, y, r) && isFillOrEdge(x - 1, y, r))
		return true;
	else
		return false;
}

